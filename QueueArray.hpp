////
//// Created by Антоша on 05.03.2020.
////
//
#ifndef BINARYTREE__QUEUEARRAY_HPP_
#define BINARYTREE__QUEUEARRAY_HPP_
#include "Queue.hpp"
template<class T>
class QueueArray : public Queue<T>
{
public:
  explicit QueueArray(int size = 100); // size задает размер "по умолчанию"
  QueueArray(const QueueArray<T> &src);
  virtual ~QueueArray()
  { delete[] array_; }
  void enQueue(const T &e);
  const T &deQueue();
  bool isEmpty()
  { return head_ == tail_; }
private:
  T *array_; // массив элементов очереди
  int head_ = 1; // Очередь пуста, если head[Q] = tail[Q].
  int tail_ = 1; // Первоначально: head[Q] = tail[Q] = 1;
  int size_; // размер очереди
};
template<class T>
QueueArray<T>::QueueArray(int size) : size_(size), head_(0), tail_(0)
{
  try
  {
    if(size <= 0)
      throw WrongQueueSize();
    array_ = new T[size_];// пытаемся заказать память под элементы
//    стека...
  }
  catch (...)
  { // если что-то случилось (например, размер слишком большой
    throw WrongQueueSize(); // или отрицательный) - возникает искл.ситуация
  }
}
template<class T>
QueueArray<T>::QueueArray(const QueueArray<T> &src)
{
  try
  {
    size_ = src.size_;
    array_ = new T[size_];
  }
  catch (...)
  {
    throw WrongQueueSize();
  }
// копирование очереди . . .
  for (int i = 0; i < src.size_ + 1; i++)
  {
    array_[i] = src.array_[i];
  }
  head_ = src.head_;
  tail_ = src.tail_;
}
template<class T>
void QueueArray<T>::enQueue(const T &e)
{
  if (head_ == tail_ + 1)
    throw QueueOverflow(); // нет места для нового элемента
  if (tail_ == size_)
  {
    tail_ = 0;
  } else
  {
    tail_++;
  }

  // занесение элемента в очередь . . .
  array_[tail_] = e;

}
template<class T>
const T &QueueArray<T>::deQueue()
{
  if (head_ == tail_)
    throw QueueUnderflow(); // очередь пуста
  if (head_ == size_)
  {
    head_ = 0;
  } else
  {
    head_++;
  }

  // удаление элемента из очереди . . .

  return array_[head_];
}

#endif //BINARYTREE__QUEUEARRAY_HPP_
