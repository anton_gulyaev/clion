#include <iostream>
#include "BinarySearchTree.hpp"
#include "QueueList.hpp"
#include "Queue.hpp"
#include <cassert>
#include "StackArray.hpp"
using namespace std;
int main()
{
  try
  {
    QueueArray<int> queueArray(2);
    QueueArray<int> queueCopyTest2(queueArray);

    const char *empty = (queueCopyTest2.isEmpty() ? "empty" : "not empty");
    cout << empty << "\n";
    queueArray.enQueue(4);
    queueArray.enQueue(5);
    QueueArray<int> queueCopyTest(queueArray);
    queueCopyTest2.enQueue(6);
    const char *emptyCopyQueue = (queueCopyTest2.isEmpty() ? "empty" : "not empty");
    cout << emptyCopyQueue << "\n";
    queueCopyTest2.enQueue(7);
    cout << queueArray.deQueue() << " array\n";
    cout << queueArray.deQueue() << " array\n";
    cout << queueCopyTest.deQueue() << " test\n";
    cout << queueCopyTest.deQueue() << " test\n";
    cout << queueCopyTest2.deQueue() << " test2\n";
    cout << queueCopyTest2.deQueue() << " test2\n" << "\n\n";

    QueueList<int> queueList;
    QueueList<int> queueListCopyTest2;

    const char *emptyList = (queueListCopyTest2.isEmpty() ? "empty" : "not empty");
    cout << emptyList << "\n";
    queueListCopyTest2.enQueue(6);
    queueList.enQueue(4);
    queueList.enQueue(5);
    QueueList<int> queueListCopyTest = std::move(queueList);

    queueListCopyTest.enQueue(8);
    queueListCopyTest.enQueue(9);

    const char *emptyCopyList = (queueListCopyTest2.isEmpty() ? "empty" : "not empty");
    cout << emptyCopyList << "\n";
    queueListCopyTest2.enQueue(7);
//    cout << queueList.deQueue() << " list\n";
//    cout << queueList.deQueue() << " list\n";
    cout << queueListCopyTest.deQueue() << " test \n";
    cout << queueListCopyTest.deQueue() << " test \n";
    cout << queueListCopyTest.deQueue() << " test \n";
    cout << queueListCopyTest.deQueue() << " test \n";
    cout << queueListCopyTest2.deQueue() << " test2 \n";
    cout << queueListCopyTest2.deQueue() << " test2 \n";
  } catch (exception &ex)
  {
    cerr << ex.what() << "\n";
  }

  BinarySearchTree<int> intkek;

  intkek.insert(6);
  intkek.insert(4);
  intkek.insert(8);
  intkek.insert(3);
  intkek.insert(7);
  intkek.insert(9);
  intkek.insert(5);
  intkek.insert(2);
  intkek.insert(10);

  BinarySearchTree<int> intTree;

  intTree.insert(6);
  intTree.insert(4);
  intTree.insert(8);
  intTree.insert(3);
  intTree.insert(7);
  intTree.insert(9);
  intTree.insert(5);
  intTree.insert(2);
  intTree.insert(10);

  cout << intTree.isSimilar(intkek) << "\n";
  string isSim = (intTree.isSimilar(intkek) ? "true" : "false");
  cout << endl << "Is similar trees? " << isSim << endl;
  intTree.insert(10);
  intkek.print(cout);
  cout << endl;
  cout << endl << "kek: ";
  intkek.horizontalWalk();
  cout << endl;
  cout << endl;

  cout << endl << "tree: ";
  intTree.horizontalWalk();
  cout << endl;
  string isSimilar = (intTree.isSimilar(intkek) ? "true" : "false");
  cout << endl << "Is similar trees? " << isSimilar << endl;

  intTree.insert(3);
  intTree.insert(2);
  intTree.insert(1);
  intTree.print(cout);
  intTree.insert(4);
  intTree.insert(1);
  intTree.insert(6);
  intTree.insert(13);
  intTree.insert(14);
  intTree.insert(4);
  intTree.print(cout);
  cout << "Count: " << intTree.getCount() << endl;
  cout << "Height: " << intTree.getHeight() << endl;
  intTree.deleteKey(15);
  intTree.print(cout);
  intTree.deleteKey(14);
  intTree.print(cout);
  intTree.deleteKey(6);
  cout << "Count: " << intTree.getCount() << endl;
  cout << "Height: " << intTree.getHeight() << endl;
  intTree.print(cout);
  cout << "Count of even numbers = " << intTree.countEven() << endl;
  intTree.deleteKey(13);
  intTree.print(cout);
  cout << "Count: " << intTree.getCount() << endl;
  cout << "Height: " << intTree.getHeight() << endl;
  (intTree.iterativeSearch(3)) ? cout << "3 is found" << endl : cout << "3 isn't found" << endl;
  intTree.deleteKey(3);
  intTree.print(cout);
  (intTree.iterativeSearch(3)) ? cout << "3 is found" << endl : cout << "3 isn't found" << endl;
  cout << "Count: " << intTree.getCount() << endl;
  cout << "Height: " << intTree.getHeight() << endl << endl;
  intTree.print(cout);

  intTree.nextNodeRight(intTree.getRoot());
  intTree.nextNodeLeft(intTree.getRoot());

  int mini = 0;
  mini = intTree.getKeySubTree(0);
  cout << "The smallest elem of intTree is " << mini << "\n";

  int otherElem = 0;
 otherElem = intTree.getKeySubTree(5);
  cout << "Another elem of intTree is " << otherElem << "\n";
  intTree.getKeySubTree(-1);

  return 0;
}
