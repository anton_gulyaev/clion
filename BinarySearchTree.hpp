//
// Created by Антоша on 01.03.2020.
//
using namespace std;
#ifndef BINATY_TREES_BINARYSEARCHTREE_HPP
#define BINATY_TREES_BINARYSEARCHTREE_HPP
#include "QueueArray.hpp"
#include "QueueList.hpp"
#include "StackList.hpp"
#include <cassert>
template<class T>
class BinarySearchTree
{
  private:
  class Node
  {
    public:
    T key_;
    Node *left_;
    Node *right_;
    explicit Node(const T key, Node *left = nullptr, Node *right = nullptr) :
      key_(key), left_(left), right_(right)
    {
    }

    void setKey(Node *current)
    {
      this->key_ = current->key_;
    }

  };

  Node *root_;

  public:

  BinarySearchTree() : root_(nullptr)
  {
  };

  virtual ~BinarySearchTree()
  {
    deleteSubtree(root_);
  }

  void print(ostream &out) const
  {
    printNode(out, root_);
    out << endl;
  }

  Node *nextNodeRight(Node *node)
  {
    Node *temp = node;
    while (node->key_ != temp->key_)
    {
      if (node->key_ > temp->key_)
      {
        temp = temp->right_;
      } else
      {
        temp = temp->left_;
      }
      if (temp == nullptr)
        return nullptr;
    }
    if (node->key_ == temp->key_)
    {
      if (temp->right_ != nullptr)
      {
        std::cout << "Key we need = " << temp->right_->key_ << ", adress " << temp->right_ << "\n";
        return temp->right_;
      }
    }
    return nullptr;
  }

  Node *getRoot()
  {
    return root_;
  }

  Node *nextNodeLeft(Node *node)
  {
    Node *temp = node;
    while (node->key_ != temp->key_)
    {
      if (node->key_ > temp->key_)
      {
        temp = temp->right_;
      } else
      {
        temp = temp->left_;
      }
      if (temp == nullptr)
        return nullptr;
    }
    if (node->key_ == temp->key_)
    {
      if (temp->left_ != nullptr)
      {
        std::cout << "Key we need = " << temp->left_->key_ << ", adress " << temp->left_ << "\n";
        return temp->left_;
      }
    }
    return nullptr;
  }

  int countOfEvenElemReq(Node *node = nullptr)
  {
    int countEven = 0;
    if (node)
    {
      if (node->key_ % 2 == 0)
      {
        countEven++;
      }
      countEven += countOfEvenElemReq(node->right_);
      countEven += countOfEvenElemReq(node->left_);
    }
    return countEven;
  }

  int countEven()
  {
    return countOfEvenElemReq(this->root_);
  }

  bool iterativeSearch(const T &key) const
  {
    return (iterativeSearchNode(key, root_) != nullptr);
  }

  void insertReq(const T &key, Node *node = nullptr)
  {
    if (root_ == nullptr)
    {
      root_ = new Node(key);
    } else
    {
      Node *temp = node;
      if (node == nullptr)
      {
        temp = root_;
      }
      if (key > temp->key_)
      {
        if (temp->right_)
        {
          insertReq(key, temp->right_);
        } else
        {
          temp->right_ = new Node(key, nullptr, nullptr);
        }
      } else if (key < temp->key_)
      {
        if (temp->left_)
        {
          insertReq(key, temp->left_);
        } else
        {
          temp->left_ = new Node(key, nullptr, nullptr);
        }
      } else
      {
        std::cout << endl << "This tree has equal key" << key << "\n";
        return;
      }
    }
  }

  void insert(const T &key)
  {
    if (root_ == nullptr)
    {
      root_ = new Node(key);
    } else
    {
      Node *temp = root_;
      while (temp->left_ || temp->right_)
      {
        if (key > temp->key_)
        {
          if (temp->right_)
          {
            temp = temp->right_;
          } else
          {
            break;
          }
        } else if (key < temp->key_)
        {
          if (temp->left_)
          {
            temp = temp->left_;
          } else
          {
            break;
          }
        } else
        {
          std::cout << endl << "This tree has equal key" << key << "\n";
          return;
        }
      }
      if ((key > temp->key_) && (!temp->right_))
      {
        temp->right_ = new Node(key, nullptr, nullptr);
      } else if ((key < temp->key_) && (!temp->left_))
      {
        temp->left_ = new Node(key, nullptr, nullptr);
      }
    }
  }

  void deleteKey(const T &key)
  {
    Node *root = iterativeSearchNode(key, this->root_);
    if (root == nullptr)
    {
      return;
    }
    if (root == root_)
    {
      deleteRoot();
      std::cout << "\nDeleted: " << root_->key_ << "\n";
      return;
    }
    Node *parent = iterativeSearchParentNode(key, this->root_);
    Node *temp = nullptr;

    if ((parent->left_) && (parent->left_->key_ == key))
    {
      temp = parent->left_;
    } else
    {
      temp = parent->right_;
    }
    if (temp)
    {
      if (!temp->left_ && !temp->right_)
      {
        if (temp->key_ > parent->key_)
        {
          parent->right_ = nullptr;
        } else
        {
          parent->left_ = nullptr;
        }
        std::cout << "\nDeleted: " << temp->key_ << "\n";
        delete temp;
      } else if (!temp->left_ || !temp->right_)
      {
        if (!temp->left_)
        {
          if (temp->key_ > parent->key_)
          {
            parent->right_ = temp->right_;
          } else
          {
            parent->left_ = temp->right_;
          }
        } else
        {
          if (temp->key_ > parent->key_)
          {
            parent->right_ = temp->left_;
          } else
          {
            parent->left_ = temp->left_;
          }
        }
        std::cout << "\nDeleted: " << temp->key_ << "\n";
        delete temp;
      } else
      {
        Node *successor = treeSuccessor(temp);
        Node *successorParent = iterativeSearchParentNode(successor->key_, this->root_);

        temp->setKey(successor);
        if (successorParent->left_ == successor)
        {
          successorParent->left_ = successor->right_;
        } else
        {
          successorParent->right_ = successor->left_;
        }
        std::cout << "\nDeleted: " << successor->key_ << "\n";
        delete successor;
      }
    }
  }

  int getCount() const
  {
    return getCountSubTree(this->root_);
  }

  int getHeight() const
  {
    return getHeightSubTree(this->root_);
  }

  void walkAtLevels()
  {
    if (!root_)
    {
      std::cout << "Tree is empty" << "\n";
      return;
    }
    QueueList<Node *> queue;
    queue.enQueue(root_);
    Node *temp = root_;
    while (!queue.isEmpty())
    {
      temp = queue.deQueue();
      cout << temp->key_ << " ";
      if (temp->left_)
      {
        queue.enQueue(temp->left_);
      }
      if (temp->right_)
      {
        queue.enQueue(temp->right_);
      }
    }
  }

  T getKeySubTree(int number)
  {
    assert(number >= 0 && root_ != nullptr);
    StackList<Node *> stack;
    stack.push(root_);
    Node *temp = root_;
    Node *lastLeft = root_;
    int index = -1;
    while (!stack.isEmpty())
    {
      while (temp->left_)
      {
        temp = temp->left_;
        stack.push(temp);
      }
      lastLeft = stack.pop();
      index++;
      if (lastLeft->right_ != nullptr)
      {
        temp = lastLeft->right_;
        stack.push(temp);
      }
      if (index == number)
      {
        return lastLeft->key_;
      }
    }
    std::cout << "Out of bounds\n";
    return -1;
  }

  bool isSimilar(BinarySearchTree<T> &tree)
  {
    if ((root_ && !tree.root_) || (!root_ && tree.root_))
    {
      std::cout << "One tree is empty" << "\n";
      return false;
    }
    if (!root_ && !tree.root_)
    {
      std::cout << "Two tree is empty" << "\n";
      return true;
    }
    StackList<Node *> stackThis;
    StackList<Node *> stackTree;
    stackThis.push(root_);
    stackTree.push(tree.root_);
    Node *tempTree = tree.root_;
    Node *tempThis = root_;
    Node *lastLeftThis = nullptr;
    Node *lastLeftTree = nullptr;
    while (!stackThis.isEmpty() && !stackTree.isEmpty())
    {
      while (tempThis->left_)
      {
        tempThis = tempThis->left_;
        stackThis.push(tempThis);
      }
      while (tempTree->left_)
      {
        tempTree = tempTree->left_;
        stackTree.push(tempTree);
      }
      lastLeftTree = stackTree.pop();
      lastLeftThis = stackThis.pop();
      if (lastLeftThis->key_ != lastLeftTree->key_)
      {
        return false;
      }
      if (lastLeftThis->right_ != nullptr)
      {
        tempThis = lastLeftThis->right_;
        stackThis.push(tempThis);
      }
      if (lastLeftTree->right_ != nullptr)
      {
        tempTree = lastLeftTree->right_;
        stackTree.push(tempTree);
      }
    }
    return stackThis.isEmpty() && stackTree.isEmpty();
  }

  void horizontalWalk()
  {
    if (!root_)
    {
      std::cout << "Tree is empty" << "\n";
      return;
    }
    StackList<Node *> stack;
    stack.push(root_);
    Node *temp = root_;
    Node *lastLeft = nullptr;
    while (!stack.isEmpty())
    {
      while (temp->left_)
      {
        temp = temp->left_;
        stack.push(temp);
      }
      lastLeft = stack.pop();
      std::cout << lastLeft->key_ << " ";
      if (lastLeft->right_ != nullptr)
      {
        temp = lastLeft->right_;
        stack.push(temp);
      }
    }
  }

  private:

  void deleteRoot()
  {
    if (!root_->left_ && !root_->right_)
    {
      root_ = nullptr;
      return;
    } else if (root_->left_ && root_->right_)
    {
      Node *successor = treeSuccessor(root_);
      Node *successorParent = iterativeSearchParentNode(successor->key_, this->root_);
      std::cout << "Deleted root: " << root_->key_ << "\n";
      root_->setKey(successor);
      if (successorParent == root_)
      {
        if (successorParent->left_ && successorParent->left_ == successor)
        {
          successorParent->left_ = successor->left_;
        } else
        {
          successorParent->right_ = successor->right_;
        }
      } else
      {
        if (successorParent->left_ && successorParent->left_ == successor)
        {
          successorParent->left_ = nullptr;
        } else
        {

          successorParent->right_ = nullptr;
        }
      }
      delete successor;
      return;
    } else
    {
      Node *temp = nullptr;
      if (!root_->right_)
      {
        temp = root_;
        root_ = root_->left_;
      } else
      {
        temp = root_;
        root_ = root_->right_;
      }

      delete temp;
    }
  }
  Node *treeSuccessorParent(Node *node)
  {
    if (node->right_)
    {
      return treeMinimumParent(node->right_);
    }
    if (node->left_)
    {
      return treeMaximumParent(node->left_);
    }
    return nullptr;
  }

  Node *treeSuccessor(Node *node)
  {
    if (node->right_)
    {
      return treeMinimum(node->right_);
    }
    if (node->left_)
    {
      return treeMaximum(node->left_);
    }
    return nullptr;
  }

  Node *treeMaximum(Node *node)
  {
    Node *temp = node;
    while (temp->right_)
    {
      temp = temp->right_;
    }
    return temp;
  }

  Node *treeMaximumParent(Node *node)
  {
    Node *temp = nullptr;
    while (node->right_)
    {
      temp = node;
      node = node->right_;
    }
    return temp;
  }

  Node *treeMinimumParent(Node *node)
  {
    Node *temp = nullptr;
    while (node->left_)
    {
      temp = node;
      node = node->left_;
    }
    return temp;
  }

  Node *treeMinimum(Node *node)   //для лефт доделать
  {
    Node *temp = node;
    while (temp->left_)
    {
      temp = temp->left_;
    }
    return temp;
  }

  int getHeightSubTree(Node *node) const
  {
    int max = 0;
    int count = 0;
    if (node)
    {
      count++;
      max++;
      max += getHeightSubTree(node->right_);
      count += getHeightSubTree(node->left_);
    }
    return (max < count) ? count : max;
  }

  Node *iterativeSearchParentNode(const T &key, Node *node) const
  {
    Node *temp = node;
    Node *current = nullptr;
    while (key != temp->key_)
    {
      if (key > temp->key_)
      {
        current = temp;
        temp = temp->right_;
      } else
      {
        current = temp;
        temp = temp->left_;
      }
      if (temp == nullptr)
        return temp;
    }
    if (key == temp->key_)
    {
      return current;
    }
    return nullptr;
  }

  Node *iterativeSearchNode(const T &key, Node *node) const
  {
    Node *temp = node;
    while (key != temp->key_)
    {
      if (key > temp->key_)
      {
        temp = temp->right_;
      } else
      {
        temp = temp->left_;
      }
      if (temp == nullptr)
        return temp;
    }
    if (key == temp->key_)
    {
      return temp;
    }
    return nullptr;
  }

  void deleteSubtree(Node *node)
  {
    if (node)
    {
      if (node->left_)
      {
        deleteSubtree(node->left_);
      }
      if (node->right_)
      {
        deleteSubtree(node->right_);
      }
      delete node;
    }
  }

  int getCountSubTree(Node *node) const
  {
    if (node == nullptr)
    {
      return 0;
    }
    return (1 + getCountSubTree(node->left_) +
      getCountSubTree(node->right_));
  }

  void printNode(ostream &out, Node *root) const
  {
    out << '(';
    if (root)
    {
      out << root->key_;
      if (root->left_)
      {
        printNode(out, root->left_);
      }
      if (root->right_)
      {
        printNode(out, root->right_);
      }
    }
    out << ')';
  }
};

#endif //BINATY_TREES_BINARYSEARCHTREE_HPP
