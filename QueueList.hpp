//
// Created by Антоша on 11.03.2020.
//

#ifndef BINARYTREE__QUEUELIST_HPP_
#define BINARYTREE__QUEUELIST_HPP_

#include "Queue.hpp"
template<class T>
class QueueList : public Queue<T>
{
  private:
  class Node
  {
    public:
    Node *next_;
    Node *prev_;
    T data_;
    Node(T data = 0, Node *next = nullptr, Node* prev = nullptr) : next_(next), data_(data), prev_(prev)
    {}
  };
  Node *head_;
  Node *tail_;
  public:
  QueueList() : head_(nullptr), tail_(nullptr)
  {}
  QueueList(QueueList<T> && queue) : head_(queue.head_), tail_(queue.tail_)
  {
    queue.head_ = nullptr;
    queue.tail_ = nullptr;
  }

  virtual ~QueueList();
  void enQueue(const T &e);
  const T &deQueue();
  bool isEmpty()
  { return head_ == nullptr; }
};

template<class T>
QueueList<T>::~QueueList()
{
  Node *temp;
  while (tail_)
  {
    temp = tail_;
    tail_ = tail_->prev_;
    delete temp;
  }
}

template<class T>
void QueueList<T>::enQueue(const T &e)
{
  if (!tail_)
  {
    head_ = new Node(e);
    tail_ = head_;
  } else
  {
    Node *temp = tail_;
    tail_ = new Node(e, nullptr, tail_);
    temp->next_ = tail_;
  }
}

template<class T>
const T &QueueList<T>::deQueue()
{
  if (!head_)
    throw QueueUnderflow();
  Node *temp = head_;
  head_ = head_->next_;
  if(!head_)
    tail_ = nullptr;
  return temp->data_;

}

#endif //BINARYTREE__QUEUELIST_HPP_
