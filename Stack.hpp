//
// Created by Антоша on 11.03.2020.
//

#ifndef BINARYTREE__STACK_HPP_
#define BINARYTREE__STACK_HPP_

#include <exception>
template<class T>
class Stack
{
public:
  virtual ~Stack()
  {}// Виртуальный - для переопределения
  // Абстрактные операции со стеком
  virtual void push(const T &e) = 0; // Добавление элемента в стек
  virtual const T &pop() = 0;// Удаление и возвращение верхнего элемента.
  // Если элементов нет, может возникнуть StackUnderflow
  virtual bool isEmpty() = 0; // Проверка стека на пустоту
};

class StackOverflow : public std::exception
{
public:
  StackOverflow() : reason("Stack Overflow")
  {}
  const char *what() const throw()
  { return reason; }
private:
  const char *reason; // ! const
};
class StackUnderflow : public std::exception
{
public:
  StackUnderflow() : reason("Stack Underflow")
  {}
  const char *what() const throw()
  { return reason; }
private:
  const char *reason; // ! const
};

class WrongStackSize : public std::exception
{
public:
  WrongStackSize() : reason("Wrong Stack Size")
  {}
  const char *what() const throw()
  { return reason; }
private:
  const char *reason; // ! const
};

#endif //BINARYTREE__STACK_HPP_
