//
// Created by Антоша on 11.03.2020.
//

#ifndef BINARYTREE__STACKARRAY_HPP_
#define BINARYTREE__STACKARRAY_HPP_

#include "Stack.hpp"

template <class T>
class StackArray : public Stack<T>
{
public:
  explicit StackArray(int size = 100); // size задает размер стека "по умолчанию"
  StackArray(const StackArray<T>& src);
  virtual ~StackArray() { delete[] array_; }
  void push(const T& e);
  const T& pop();
  bool isEmpty() { return top_ == 0; }
  T getTop();
private:
  T* array_; // массив элементов стека:
  int top_ = 0; // вершина стека, элемент занесенный в стек последним
  int size_; // размер стека
};
template <class T>
StackArray<T>::StackArray(int size) {
  try {
    if(size <= 0)
      throw WrongStackSize();
    size_ = size;
    array_ = new T[size + 1];
    // пытаемся заказать память под элементы
    //стека...
  }
  catch (...) { // если что-то случилось (например, размер слишком
    // большой
    throw WrongStackSize(); // или отрицательный) - возникает исключительная
    // ситуация
  }
  top_ = 0;
}
template <class T>
StackArray<T>::StackArray(const StackArray<T>& src) {
  try {
    size_ = src.size_;
    array_ = new T[src.size_ + 1];
  }
  catch (...) {
    throw WrongStackSize();
  }
  // копирование массива
  for (int i = 0; i < src.top_ + 1; i++) {
    array_[i] = src.array_[i];
  }
  top_ = src.top_;
}
template <class T>
void StackArray<T>::push(const T& e)
{
  if (top_ == size_ + 1)
    throw StackOverflow(); // нет места для нового элемента
  top_++;
  array_[top_] = e; // занесение элемента в стек
}
template <class T>
const T& StackArray<T>::pop()
{
  if (top_ == 0)
    throw StackUnderflow(); // стек пуст
  return array_[top_--]; // Элемент физически остается в стеке, но больше "не
  // доступен"
}

template <class T>
T StackArray<T>::getTop()
{
  if (top_ == 0)
    throw StackUnderflow();
  if (top_ == size_)
    throw StackOverflow();
  return array_[top_];// (top != 0 && top_ != size_) ? top_ : 0;
}


#endif //BINARYTREE__STACKARRAY_HPP_
