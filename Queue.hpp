//
// Created by Антоша on 05.03.2020.
//

#ifndef BINARYTREE__QUEUE_HPP_
#define BINARYTREE__QUEUE_HPP_
#include <exception>
template<class T>
class Queue
{
public:
  virtual ~Queue()
  {}// Виртуальный - для переопределения
// Абстрактные операции со стеком
  virtual void enQueue(const T &e) = 0; // Добавление элемента в стек
  virtual const T &deQueue() = 0;// Удаление и возвращение верхнего элемента.
  // Если элементов нет, может возникнуть QueuekUnderflow
  virtual bool isEmpty() = 0; // Проверка на пустоту
};

class QueueUnderflow : public std::exception
{
public:
  QueueUnderflow() : reason("Queue Underflow")
  {}
  const char *what() const throw()
  { return reason; }
private:
  const char *reason; // ! const
};

class WrongQueueSize : public std::exception
{
public:
  WrongQueueSize() : reason("Wrong Queue Size")
  {}
  const char *what() const throw()
  { return reason; }
private:
  const char *reason; // ! const
};
//
//
class QueueOverflow : public std::exception
{
public:
  QueueOverflow() : reason("Queue Overflow")
  {}
  const char *what() const throw()
  { return reason; }
private:
  const char *reason; // ! const
};



#endif //BINARYTREE__QUEUE_HPP_
