//
// Created by Антоша on 11.03.2020.
//

#ifndef BINARYTREE__STACKLIST_HPP_
#define BINARYTREE__STACKLIST_HPP_

#include "Stack.hpp"
//hui
template<class T>
class StackList : public Stack<T>
{
  private:
  class Node
  {
    public:
    Node* next_;
    Node* prev_;
    T data_;
    Node(T data = 0, Node* next = nullptr, Node* prev = nullptr) : next_(next), data_(data), prev_(prev)
    {}
  };
  Node* head_;
  Node* tail_;
  public:
  StackList() : head_(nullptr)
  {}
  StackList(StackList<T>&& list) : head_(list.head_), tail_(list.tail_)
  {
    list.head_ = nullptr;
    list.tail_ = nullptr;
  }
  virtual ~StackList();
  void push(const T& e);
  const T& pop();
  bool isEmpty()
  {
    return head_ == nullptr;
  }
};

template<class T>
StackList<T>::~StackList()
{
  Node* temp;

  while (tail_)
  {
    temp = tail_;
    tail_ = tail_->prev_;
    delete temp;
  }
}

template<class T>
void StackList<T>::push(const T& e)
{
  if(!head_)
  {
    tail_ = new Node(e, head_);
    head_ = tail_;
  } else{
    Node* temp = new Node(e, head_, head_->prev_);
    head_ = temp;
  }
}

template<class T>
const T& StackList<T>::pop()
{
  if (!head_)
    throw StackUnderflow();
  Node* temp = head_;
  head_ = head_->next_;
  return temp->data_;
}

#endif //BINARYTREE__STACKLIST_HPP_
